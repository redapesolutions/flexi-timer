# Flexi Timer

A timer package primarily focus on flexibility with controller.

## Getting Started

Add **flexi_timer** under dependencies in pubspec.yaml:
```yaml
dependencies:
  flexi_timer:
```

Run the following command:
```yaml
$ flutter pub get
```

Import the package:
```
import 'package:flexi_timer/flexi_timer.dart';
```

## Usage
Users can either start the timer **automatically** or **via controller**. The following will guide you through the basic of the two approaches.

### Initialise controller
Firstly, initialise the controller and pass the controller like so:
```dart
FlexiTimerController _timerController = FlexiTimerController();
```
```dart
FlexiTimer(
  controller: _timerController,
  timerFormat: TimerTextFormat.ms,
  ...
);
```

### First approach: Manually start timer via controller

Call the startCountdown() function and pass the duration
```dart
_timerController.startCountdown(Duration(seconds: 120));
```

### Second approach: Auto-start timer by passing duration to FlexiTimer widget

Specify the duration in the FlexiTimer widget
```dart
FlexiTimer(
  controller: _timerController,
  timerFormat: TimerTextFormat.ms,
  duration: const Duration(seconds: 5),
  ...
);
```
### Stop timer via controller

Stop the timer by calling stop() function
```dart
_timerController.stop();
```

### Brief overview

```dart
FlexiTimer(
  controller: _timerController,
  duration: const Duration(seconds: 5),
  timerFormat: TimerTextFormat.ms,
  onComplete: () {
    print('Timer completed!');
  },
  textStyle: const TextStyle(
    color: Colors.blue,
    fontSize: 60,
  ),
  isDiplayIcon: true,
  iconPosition: IconPosition.prefix,
  icon: const Icon(
    Icons.timer,
  ),
  iconPadding: 10,
  iconColor: Colors.blue,
  iconSize: 50,
);
```

## Parameters
| Name          | Type                 | Default Value       | Mandatory | Description                                                                                                                                               |
|---------------|----------------------|---------------------|-----------|-----------------------------------------------------------------------------------------------------------------------------------------------------------|
| onComplete    |     VoidCallback?    |         null        |     N     | This callback will be executed after timed out                                                                                                            |
| textStyle     |      TextStyle      | TextStyle (color: Colors.black, fontSize: 24) |     N     | This param allows users to style the text of timer                                                                                                        |
| timerFormat   |    TimerTextFormat   | TimerTextFormat.s |     N     | This param allows users to change the format of timer with the following options: <br> • TimerTextFormat.Hms <br> • TimerTextFormat.ms <br> • TimerTextFormat.s |
| icon          |         Icon?        | Icon(Icons.timer, size: widget.iconSize, color: widget.iconColor) |     N     | This param allows users to personalise the Icon widget                                                                                                    |
| iconPadding   |        double        |          10         |     N     | This param allows users to change the padding between the icon and the timer text                                                                         |
| iconSize      |        double?       |          24        |     N     | This param allows users to change the default icon size                                                                                                   |
| iconColor     |        Color?        | Colors.black |     N     | This param allows users to change the default icon color                                                                                                  |
| iconPosition  |     IconPosition     | IconPosition.suffix |     N     | This param allows users to change the position of icon with the following options: <br> • IconPosition.prefix <br> • IconPosition.suffix                            |
| isDisplayIcon |         bool         |        false        |     N     | This param allows users to control the visibility of icon                                                                                                 |
| controller    | FlexiTimerController |         null        |     Y     | This param allows users to control the behaviour of timer                                                                                                 |
| duration      |       Duration?      |         null        |     N     | This param allows user to personalise the duration of timer                                                                                               |                                                                                         |

## Demo 
![demo](https://gitlab.com/redapesolutions/flexi-timer/-/raw/main/demo/demo.gif)
