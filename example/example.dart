import 'package:flexi_timer/flexi_timer.dart';
import 'package:flexi_timer/src/enums/icon_position.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Flexi Timer Demo'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    FlexiTimerController _timerController = FlexiTimerController();
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            FlexiTimer(
              controller: _timerController,
              duration: const Duration(seconds: 5),
              timerFormat: TimerTextFormat.Hms,
              onComplete: () {
                print('Timer completed!');
              },
              textStyle: const TextStyle(
                color: Colors.blue,
                fontSize: 60,
              ),
              isDiplayIcon: true,
              iconPosition: IconPosition.prefix,
              icon: const Icon(
                Icons.timer,
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                TextButton(
                  child: const Text(
                    "Start",
                    style: TextStyle(fontSize: 30),
                  ),
                  onPressed: () {
                    _timerController.startCountdown(
                      const Duration(seconds: 5),
                    );
                  },
                ),
                TextButton(
                  child: const Text(
                    "Stop",
                    style: TextStyle(fontSize: 30),
                  ),
                  onPressed: () {
                    _timerController.stop();
                  },
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
